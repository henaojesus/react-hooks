/* COMPONENTS */
import Header from "./components/Header";
import Characters from './components/Characters';
/* CSS */
import './App.css';


function App() {
  return (
    <div className="App">
      <Header/>
      <Characters/>
      <h1>Hola Mundo</h1>
    </div>
  );
}

export default App;
